#!/bin/sh

if [ $# -eq 1 ]; then
  if [ "$1" == "--latest" ]; then
    latest="1"
  fi
fi

tag="$BUILD_VERSION"
echo "pushing tagged version"
docker push registry.gitlab.com/rpk788/bungie-manifest-updater:$tag

if [ "$latest" == "1" ]; then
  echo "Also requesting push latest"
  docker push registry.gitlab.com/rpk788/bungie-manifest-updater:latest
fi

exit $?