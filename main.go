package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/debug"

	"golang.org/x/sync/errgroup"

	"gitlab.com/rpk788/bungie-manifest-updater/db"
	"gitlab.com/rpk788/bungie-manifest-updater/manifest"
)

var (
	buildVersionString string
)

func init() {
	initVersionMetadata()
}

func initVersionMetadata() {
	version := "unknown"

	if os.Getenv("BUILD_VERSION") != "" {
		buildVersionString = os.Getenv("BUILD_VERSION")
		return
	}

	revision := ""
	buildTime := ""
	if buildInfo, ok := debug.ReadBuildInfo(); ok {
		settings := buildInfo.Settings
		for _, setting := range settings {
			if setting.Key == "vcs.time" {
				buildTime = setting.Value
			}
			if setting.Key == "vcs.revision" {
				revision = setting.Value
			}
		}
	}

	shortHash := ""
	if buildTime == "" && revision == "" {
		buildVersionString = version
		return
	}
	if revision != "" {
		shortHash = revision[:7]
	}

	buildVersionString = fmt.Sprintf("%s-%s", buildTime, shortHash)
}

func main() {

	withItems := flag.Bool("items", false, "Use this to request item entries be parsed from the manifest")
	withAssets := flag.Bool("assets", false, "Use this flag to request assets be parsed")
	limit := flag.Int("limit", -1, "Use to limit the number of items imported, useful for platforms where row limits are a thing on free tiers. (looking at you Heroku)")
	flag.Parse()

	fmt.Printf("Running version=(%s)\n", buildVersionString)

	manifestSpec, err := manifest.ReadManifestSpec()
	if err != nil {
		fmt.Printf("Error requesting manfiest spec from Bungie: %s\n", err.Error())
		return
	}

	// manifestChecksums stores the md5sums of the manifest files keyed by the language they
	// are associated with.
	manifestChecksums, err := db.ReadManifestChecksums()
	if err != nil {
		fmt.Println("Error trying to read manifest checksums")
		return
	}

	if *withAssets {
		log.Println("Processing assets...")
		processAssets(manifestSpec)
	}
	if *withItems {
		log.Println("Processing items...")
		didUpdate, err := processItems(manifestSpec, manifestChecksums, *limit)
		if err != nil {
			log.Printf("Error updating: %s\n", err.Error())
			os.Exit(1)
		} else if !didUpdate {
			// Indicate that nothing was updated
			os.Exit(3)
		}
	}

	log.Println("Updated")
}

func processAssets(manifestSpec *manifest.SpecResponse) {

	zippedDBName := manifest.AssetsZipFilename()
	// TODO: This 2 here is hardcoded. the manifest spec has some weird entries for assets.
	// This should be removed later or possibly changed to a different index at some point.
	resourcePath := manifestSpec.Response.MobileGearAssetDataBases[2].Path
	sqlitePath := manifest.DownloadUnzippedManifestDB("/tmp", resourcePath, "", zippedDBName)
	fmt.Println(sqlitePath)
	defer os.Remove(sqlitePath)

	err := processGearAssetsManifestDB(sqlitePath)
	if err != nil {
		fmt.Printf("Error processing gear assets manifest DB: %s\n", err.Error())
		return
	}
}

func processGearAssetsManifestDB(sqlitePath string) error {

	in, err := db.GetInputDBConnection(sqlitePath)
	if err != nil {
		return err
	}
	//defer in.Database.Close()
	_, err = db.GetOutputDBConnection()
	if err != nil {
		return err
	}

	rows, err := in.GetGearAssetsDefinition()
	if err != nil {
		return err
	}
	defer rows.Close()

	manifestRows := make([]*manifest.Row, 0, 100)
	for rows.Next() {
		row := &manifest.Row{}
		err = rows.Scan(&row.ID, &row.JSON)
		if err != nil {
			fmt.Printf("Failed to scan input from assets table: %s\n", err.Error())
		}

		manifestRows = append(manifestRows, row)
	}

	err = db.Output.DumpGearAssetsDefinitions(manifestRows)
	fmt.Printf("Processed %d asset definitions...\n", len(manifestRows))

	return err
}

func processItems(manifestSpec *manifest.SpecResponse, currentChecksums map[string]string, rowLimit int) (bool, error) {

	didUpdate := false
	for lang, path := range manifestSpec.Response.MobileWorldContentPaths {
		if lang != "en" {
			// For now, only parsing the english items manifest
			continue
		}

		fmt.Println("Checking manifest for language: ", lang)
		currentChecksum := currentChecksums[lang]
		incomingChecksum := manifest.ChecksumFromManifestFilename(path)
		if currentChecksum != "" && currentChecksum == incomingChecksum {
			fmt.Println("Incoming manifest is the same as the current one already stored...skipping!")
			continue
		}

		zippedDBName := manifest.WorldContentZipName(lang)
		sqlitePath := manifest.DownloadUnzippedManifestDB("/tmp", path, lang, zippedDBName)
		defer os.Remove(sqlitePath)

		err := processWorldContentsManifestDB(lang, incomingChecksum, sqlitePath, rowLimit)
		if err != nil {
			fmt.Printf("Failed to process items database for lang(%s): %s\n", lang, err.Error())
			return false, err
		}

		didUpdate = true
	}

	return didUpdate, nil
}

// processManifestDB will process the manifest sqlite database for the specified Locale,
// reading the desired fields out of the manifest and inserting them into the new relational DB.
// The checksum provided is the md5 of the SQLite database file being processed. This should
// be stored when the new table is written to provide caching support next time.
func processWorldContentsManifestDB(locale, checksum, sqlitePath string, itemLimit int) error {

	in, err := db.GetInputDBConnection(sqlitePath)
	if err != nil {
		fmt.Println("Error opening the input database: ", err.Error())
		return err
	}
	//defer in.Database.Close()
	_, err = db.GetOutputDBConnection()
	if err != nil {
		fmt.Println("Error opening output database: ", err.Error())
		return err
	}

	errGroup := errgroup.Group{}

	// DestinyInventoryItemDefinitions
	errGroup.Go(func() error { return parseItemDefinitions(in, locale, checksum, itemLimit) })

	// DestinyInventoryBucketDefinition
	errGroup.Go(func() error { return parseBucketDefinitions(in, locale, checksum) })

	errGroup.Go(func() error {
		// Share a single transaction for all activity related tables as they have foreign key constraints
		// that need to be removed/added to import the new data.

		// NOTE: Activity constraints are being really problematic, I'm going to
		// ignore foreign keys and just handle the activities missing in code if
		// I ever get around to even using them (they are not currently).

		// err = db.Output.OpenTransaction()
		// if err != nil {
		// 	panic("Failed to open activities transaction: " + err.Error())
		// }
		// err = db.Output.RemoveActivityConstraints()
		// if err != nil {
		// 	return err
		// }
		// err = db.Output.CommitTransaction()
		// if err != nil {
		// 	return err
		// }

		err = db.Output.OpenTransaction()
		if err != nil {
			panic("Failed to open activities transaction: " + err.Error())
		}
		err = parseActivityModifiers(in, locale, checksum)
		if err != nil {
			return err
		}
		err = parseActivityTypes(in, locale, checksum)
		if err != nil {
			return err
		}
		err = parseActivityModes(in, locale, checksum)
		if err != nil {
			return err
		}
		err = parseRecords(in, locale, checksum)
		if err != nil {
			return err
		}
		err = parsePlaces(in, locale, checksum)
		if err != nil {
			return err
		}
		err = parseDestinations(in, locale, checksum)
		if err != nil {
			return err
		}
		err = parseActivities(in, locale, checksum)
		if err != nil {
			return err
		}
		err = db.Output.CommitTransaction()
		if err != nil {
			return err
		}

		// NOTE: Activity constraints are being really problematic, I'm going to
		// ignore foreign keys and just handle the activities missing in code if
		// I ever get around to even using them (they are not currently).

		// err = db.Output.OpenTransaction()
		// if err != nil {
		// 	panic("Failed to open activities transaction: " + err.Error())
		// }
		// err = db.Output.AddActivityConstraints()
		// err = db.Output.CommitTransaction()
		// if err != nil {
		// 	panic("Failed to commit activities transaction: " + err.Error())
		// }
		return err
	})

	err = errGroup.Wait()
	if err == nil {
		db.Output.SaveManifestChecksum(locale, checksum)
	}

	return err
}

func parseItemDefinitions(inputDB *db.InputDB, locale, checksum string, itemLimit int) error {
	inRows, err := inputDB.GetItemDefinitions()
	if err != nil {
		fmt.Println("Error reading item definitions from sqlite: ", err.Error())
		return err
	}
	defer inRows.Close()

	itemDefs := make([]*manifest.ItemDefinition, 0)
	for inRows.Next() {
		row := manifest.Row{}
		inRows.Scan(&row.ID, &row.JSON)

		item := manifest.ItemDefinition{}
		json.Unmarshal([]byte(row.JSON), &item)

		itemDefs = append(itemDefs, &item)

		if itemLimit != -1 && len(itemDefs) == itemLimit {
			break
		}
	}

	fmt.Printf("Processed %d item definitions\n", len(itemDefs))

	err = db.Output.DumpNewItemDefinitions(locale, checksum, itemDefs)

	return err
}

func parseBucketDefinitions(inputDB *db.InputDB, locale, checksum string) error {

	bucketRows, err := inputDB.GetBucketDefinitions()
	if err != nil {
		fmt.Println("Error reading item definitions from sqlite: ", err.Error())
		return err
	}
	defer bucketRows.Close()

	bucketDefs := make([]*manifest.BucketDefinition, 0)
	for bucketRows.Next() {
		row := manifest.Row{}
		bucketRows.Scan(&row.ID, &row.JSON)

		bucket := manifest.BucketDefinition{}
		json.Unmarshal([]byte(row.JSON), &bucket)

		bucketDefs = append(bucketDefs, &bucket)
	}

	fmt.Printf("Processed %d bucket definitions\n", len(bucketDefs))

	return db.Output.DumpNewBucketDefinitions(locale, checksum, bucketDefs)
}

func parseActivityModifiers(inputDB *db.InputDB, locale, checksum string) error {

	modifierRows, err := inputDB.GetActivityModifierDefinitions()
	if err != nil {
		fmt.Println("Error reading item definitions from sqlite: ", err.Error())
		return err
	}
	defer modifierRows.Close()

	modifierDefs := make([]*manifest.ActivityModifierDefinition, 0)
	for modifierRows.Next() {
		row := manifest.Row{}
		modifierRows.Scan(&row.ID, &row.JSON)

		modifier := manifest.ActivityModifierDefinition{}
		json.Unmarshal([]byte(row.JSON), &modifier)

		modifierDefs = append(modifierDefs, &modifier)
	}

	fmt.Printf("Processed %d activity modifier definitions\n", len(modifierDefs))

	return db.Output.DumpNewActivityModifierDefinitions(locale, checksum, modifierDefs)
}

func parseActivityTypes(inputDB *db.InputDB, locale, checksum string) error {

	activityTypeRows, err := inputDB.GetActivityTypeDefinitions()
	if err != nil {
		fmt.Println("Error reading activity types from sqlite: ", err.Error())
		return err
	}
	defer activityTypeRows.Close()

	activityTypeDefs := make([]*manifest.ActivityTypeDefinition, 0)
	for activityTypeRows.Next() {
		row := manifest.Row{}
		activityTypeRows.Scan(&row.ID, &row.JSON)

		activityType := manifest.ActivityTypeDefinition{}
		json.Unmarshal([]byte(row.JSON), &activityType)

		activityTypeDefs = append(activityTypeDefs, &activityType)
	}

	fmt.Printf("Processed %d activity type definitions\n", len(activityTypeDefs))

	return db.Output.DumpNewActivityTypeDefinitions(locale, checksum, activityTypeDefs)
}

func parseActivityModes(inputDB *db.InputDB, locale, checksum string) error {

	activityModeRows, err := inputDB.GetActivityModeDefinitions()
	if err != nil {
		fmt.Println("Error reading activity mode definitions from sqlite: ", err.Error())
		return err
	}
	defer activityModeRows.Close()

	activityModeDefs := make([]*manifest.ActivityModeDefintion, 0)
	for activityModeRows.Next() {
		row := manifest.Row{}
		activityModeRows.Scan(&row.ID, &row.JSON)

		mode := manifest.ActivityModeDefintion{}
		json.Unmarshal([]byte(row.JSON), &mode)

		activityModeDefs = append(activityModeDefs, &mode)
	}

	fmt.Printf("Processed %d activity mode definitions\n", len(activityModeDefs))

	return db.Output.DumpNewActivityModeDefinitions(locale, checksum, activityModeDefs)
}

func parseActivities(inputDB *db.InputDB, locale, checksum string) error {

	activityRows, err := inputDB.GetActivityDefinitions()
	if err != nil {
		fmt.Println("Error reading activity definitions from sqlite: ", err.Error())
		return err
	}
	defer activityRows.Close()

	activityDefs := make([]*manifest.ActivityDefinition, 0)
	for activityRows.Next() {
		row := manifest.Row{}
		activityRows.Scan(&row.ID, &row.JSON)

		activity := manifest.ActivityDefinition{}
		json.Unmarshal([]byte(row.JSON), &activity)

		activityDefs = append(activityDefs, &activity)
	}

	fmt.Printf("Processed %d activity definitions\n", len(activityDefs))

	return db.Output.DumpNewActivityDefinitions(locale, checksum, activityDefs)
}

func parsePlaces(inputDB *db.InputDB, locale, checksum string) error {

	placeRows, err := inputDB.GetPlaceDefinitions()
	if err != nil {
		fmt.Println("Error reading place definitions from sqlite: ", err.Error())
		return err
	}
	defer placeRows.Close()

	placeDefs := make([]*manifest.PlaceDefinition, 0)
	for placeRows.Next() {
		row := manifest.Row{}
		placeRows.Scan(&row.ID, &row.JSON)

		place := manifest.PlaceDefinition{}
		json.Unmarshal([]byte(row.JSON), &place)

		placeDefs = append(placeDefs, &place)
	}

	fmt.Printf("Processed %d place definitions\n", len(placeDefs))

	return db.Output.DumpNewPlaceDefinitions(locale, checksum, placeDefs)
}

func parseDestinations(inputDB *db.InputDB, locale, checksum string) error {

	destinationRows, err := inputDB.GetDestinationDefinitions()
	if err != nil {
		fmt.Println("Error reading destination defintions from sqlite: ", err.Error())
		return err
	}
	defer destinationRows.Close()

	destinationDefs := make([]*manifest.DestinationDefintion, 0)
	for destinationRows.Next() {
		row := manifest.Row{}
		destinationRows.Scan(&row.ID, &row.JSON)

		destination := manifest.DestinationDefintion{}
		json.Unmarshal([]byte(row.JSON), &destination)

		destinationDefs = append(destinationDefs, &destination)
	}

	fmt.Printf("Processed %d destination definitions\n", len(destinationDefs))

	return db.Output.DumpNewDestinationDefinitions(locale, checksum, destinationDefs)
}

func parseRecords(inputDB *db.InputDB, locale, checksum string) error {

	recordRows, err := inputDB.GetRecordDefinitions()
	if err != nil {
		fmt.Println("Error reading record definitions from sqlite: ", err.Error())
		return err
	}
	defer recordRows.Close()

	recordDefs := make([]*manifest.RecordDefinition, 0)
	for recordRows.Next() {
		row := manifest.Row{}
		recordRows.Scan(&row.ID, &row.JSON)

		record := manifest.RecordDefinition{}
		json.Unmarshal([]byte(row.JSON), &record)

		recordDefs = append(recordDefs, &record)
	}

	fmt.Printf("Processed %d record definitions\n", len(recordDefs))

	return db.Output.DumpNewRecordDefinitions(locale, checksum, recordDefs)
}
