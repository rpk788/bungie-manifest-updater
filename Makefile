BUILD_DATE := `date +%Y-%m-%d\ %H:%M`
VERSIONFILE := version.go
APP_NAME := "bungie-manifest-updater"

all: build
build:
	go build
test:
	go test -v ./...
install:
	go install
clean:
	rm $(APP_NAME)
