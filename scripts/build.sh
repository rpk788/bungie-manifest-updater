#!/bin/sh

if [ $# -eq 1 ]; then
  if [ "$1" == "--latest" ]; then
    latest="1"
  fi
fi

tag="$BUILD_VERSION"
if [ "$latest" == "1" ]; then
  echo "Also requesting build latest"
  tags="-t registry.gitlab.com/rpk788/bungie-manifest-updater:$tag -t registry.gitlab.com/rpk788/bungie-manifest-updater:latest"
else
  tags="-t registry.gitlab.com/rpk788/bungie-manifest-updater:$tag"
fi

echo "Building tags: $tags"
docker build $tags .

exit $?

#docker run -it -e PORT=8181 -p 8181:8181 -e DATABASE_URL=$DATABASE_URL -e BUNGIE_API_KEY=$BUNGIE_API_KEY registry.gitlab.com/rpk788/bungie-manifest-updater:$(cd ./scripts && bash generate_version.sh)
