package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"github.com/pkg/errors"
	"gitlab.com/rpk788/bungie-manifest-updater/db"
	"gitlab.com/rpk788/bungie-manifest-updater/manifest"
)

var (
	version   = "0.1.0"
	buildDate = "2019-07-29"
)

func main() {

	outPath := flag.String("out-path", "", "The output path where the fixed up zipped manifest should be written")
	flag.Parse()

	if *outPath == "" {
		fmt.Println("Forgot to specify an output path.")
		flag.Usage()
		return
	}

	fmt.Printf("Running version=(%s) build on date=(%s)\n", version, buildDate)

	manifestSpec, err := manifest.ReadManifestSpec()
	if err != nil {
		fmt.Printf("Error requesting manfiest spec from Bungie: %s\n", err.Error())
		return
	}

	// read the manifest checksums from the local disk
	manifestChecksums, err := readChecksums(*outPath)
	if err != nil {
		fmt.Printf("Error trying to read manifest checksums - %s\n", err.Error())
		return
	}

	err = updateManifests(manifestSpec, manifestChecksums, *outPath)
	if err != nil {
		fmt.Printf("There was an error updating the manifests: %s\n", err.Error())
	}
}

// Checksums will store a lookup of the currently exported manifest checksums to see if they need to be updated
type Checksums map[string]string

func readChecksums(dir string) (Checksums, error) {

	result := Checksums{}
	fullPath := path.Join(dir, "checksums.json")

	contents, err := ioutil.ReadFile(fullPath)
	if err != nil {
		if os.IsNotExist(err) {
			return result, nil
		}
		return result, err
	}

	err = json.Unmarshal(contents, &result)
	return result, err
}

func writeChecksums(c Checksums, dir string) error {

	contents, err := json.Marshal(c)
	if err != nil {
		return err
	}

	fullPath := path.Join(dir, "checksums.json")
	err = ioutil.WriteFile(fullPath, contents, 0644)
	return err
}

func updateManifests(spec *manifest.SpecResponse, checksums Checksums, outPath string) error {

	for lang, path := range spec.Response.MobileWorldContentPaths {
		if lang != "en" {
			// Only support for english right now
			continue
		}

		fmt.Println("Checking manifest for language: ", lang)
		currentChecksum := checksums[lang]
		incomingChecksum := manifest.ChecksumFromManifestFilename(path)
		if currentChecksum != "" && currentChecksum == incomingChecksum {
			fmt.Println("Incoming manifest is the same as the current one already stored...skipping!")
			continue
		}

		err := updateManifest(lang, path, outPath)
		if err != nil {
			return err
		}

		checksums[lang] = incomingChecksum
	}

	err := writeChecksums(checksums, outPath)
	if err != nil {
		return err
	}

	return nil
}

func updateManifest(locale, resourcePath, outPath string) error {

	localPath := manifest.DownloadUnzippedManifestDB("/tmp", resourcePath, locale, manifest.WorldContentZipName(locale))
	//defer os.Remove(localPath)

	in, err := db.GetInputDBConnection(localPath)
	if err != nil {
		return err
	}

	tableNames, err := in.Tables()
	if err != nil {
		return err
	}

	for _, t := range tableNames {

		err = fixupTable(in, t)
		if err != nil {
			return err
		}
	}

	// Re-zip the sqlite database and move it to the output path
	err = manifest.ZipManifest(localPath, path.Join(outPath, "manifest.zip"))
	if err != nil {
		return err
	}

	// Remove the unzipped version
	err = os.Remove(localPath)
	if err != nil {
		return err
	}

	return nil
}

func fixupTable(inputDB *db.InputDB, name string) error {

	// This table doesn't seem to have an id column, just skip it for now
	if name == "DestinyHistoricalStatsDefinition" {
		fmt.Println("Skipping table DestinyHistoricalStatsDefinition...")
		return nil
	}

	fmt.Printf("Updating schema for table %s\n", name)
	err := inputDB.UpdateTableSchema(name)
	if err != nil {
		return errors.Wrap(err, "updating schema for table="+name+" failed")
	}

	err = inputDB.ExtractInterestingFields(name)
	if err != nil {
		return errors.Wrap(err, "extracting interesting fields failed for table="+name)
	}

	return nil
}
