module gitlab.com/rpk788/bungie-manifest-updater

go 1.19

require (
	github.com/lib/pq v0.0.0-20170810061220-e42267488fe3
	github.com/mattn/go-sqlite3 v1.2.1-0.20170901084005-05548ff55570
	github.com/pkg/errors v0.8.2-0.20190227000051-27936f6d90f9
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
)

require golang.org/x/net v0.0.0-20170828231752-66aacef3dd8a // indirect
