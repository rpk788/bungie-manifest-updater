package manifest

import (
	"archive/zip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"
)

const (
	// BaseURL is the base of all URLs for requesting data from Bungie.
	BaseURL = "https://www.bungie.net"
	// ManifestURL is the URL for the manifest spec that will point to the individual manifest DBs.
	ManifestURL = BaseURL + "/Platform/Destiny2/Manifest/"
)

// WorldContentZipName provides the name of the zipped world content manifest database for the supplied locale.
func WorldContentZipName(locale string) string {
	return fmt.Sprintf("world_sql_content_%s.content", locale)
}

// AssetsZipFilename provides the filename inside the zipped manifest for the assets database.
func AssetsZipFilename() string {
	return "asset_sql_content.content"
}

// ReadManifestSpec will read the manifest spec that contains the URLs to the individual manifest
// databases. The databases are sent as zipped sqlite databases.
func ReadManifestSpec() (*SpecResponse, error) {
	client := http.DefaultClient
	req, _ := http.NewRequest("GET", ManifestURL, nil)

	bungieAPIKey := os.Getenv("BUNGIE_API_KEY")
	if bungieAPIKey != "" {
		// Providing an API Key will decrease the chances of the request being throttled
		req.Header.Add("X-Api-Key", bungieAPIKey)
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.New("Failed to make request to Bungie for the manifest spec")
	}
	defer resp.Body.Close()

	manifestSpec := &SpecResponse{}
	err = json.NewDecoder(resp.Body).Decode(manifestSpec)
	if err != nil {
		return nil, err
	}

	if manifestSpec.ErrorStatus != "Success" || manifestSpec.Message != "Ok" {
		return nil, fmt.Errorf("Error response from Bungie for manifest spec - %s, %s", manifestSpec.ErrorStatus, manifestSpec.Message)
	}

	return manifestSpec, nil
}

// ChecksumFromManifestFilename parses the md5sum value out of the manifest path
// example path: /common/destiny_content/sqlite/en/world_sql_content_7d6b460360f589e94baeb8308cada327.content
func ChecksumFromManifestFilename(path string) string {
	underscoreIndex := strings.LastIndex(path, "_")
	dotIndex := strings.LastIndex(path, ".")
	if dotIndex == -1 || underscoreIndex == -1 || dotIndex < underscoreIndex {
		return ""
	}

	return path[underscoreIndex+1 : dotIndex-1]
}

// DownloadUnzippedManifestDB will download and unzip the world content database,
// unzip it, and save the sqlite database somewhere on disk. The return value
// is the location on disk where the extracted sqlite database is saved.
func DownloadUnzippedManifestDB(localDir, resourcePath, language, zippedDBName string) string {

	client := http.DefaultClient
	req, _ := http.NewRequest("GET", BaseURL+resourcePath, nil)

	strings.TrimSuffix(resourcePath, ".content")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to download manifest for lanuage %s: %s\n", language, err.Error())
		return ""
	} else if resp.StatusCode == 304 {
		fmt.Printf("Manifest file for language %s has not changed... skipping!\n", language)
		return ""
	}
	defer resp.Body.Close()

	// Download the zipped content
	zipPath := zippedDBName
	output, err := os.OpenFile(zipPath, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Failed to open output file for writing: ", err.Error())
		return ""
	}
	defer output.Close()

	_, err = io.Copy(output, resp.Body)
	if err != nil {
		fmt.Println("Failed to write zipped content to disk: ", err.Error())
		return ""
	}
	defer os.Remove(zipPath)

	// Unzip the file
	zipReader, err := zip.OpenReader(zipPath)
	if err != nil {
		fmt.Println("Failed to read zip file that was written to disk: ", err.Error())
		return ""
	}
	defer zipReader.Close()

	if len(zipReader.File) > 1 {
		fmt.Println("Uh Oh, found more than one file in the manifest zip... ignoring all but the first.")
	}

	sqliteName := path.Join(localDir, zipReader.File[0].Name)
	zipF, _ := zipReader.File[0].Open()

	sqliteOutput, err := os.OpenFile(sqliteName, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Error opening sqlite file to write to disk: ", err.Error())
		return ""
	}
	defer sqliteOutput.Close()
	defer zipF.Close()

	io.Copy(sqliteOutput, zipF)

	// Return the path to the unzipped SQLite DB
	return sqliteName
}

// ZipManifest will compress the manifest at the supplied path and write it as a new zip file.
func ZipManifest(manifestPath, outputZipPath string) error {

	filename := path.Base(manifestPath)
	zipOutput, err := os.OpenFile(outputZipPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer zipOutput.Close()

	zipWriter := zip.NewWriter(zipOutput)
	manifestWriter, err := zipWriter.Create(filename)
	if err != nil {
		return err
	}
	defer zipWriter.Close()

	manifestReader, err := os.OpenFile(manifestPath, os.O_RDONLY, 0644)
	if err != nil {
		return err
	}
	defer manifestReader.Close()

	io.Copy(manifestWriter, manifestReader)

	return nil
}
