package manifest

// SpecResponse is the response from the public manifest endpoint provided by Bungie.
// It provides links to all of the content files which are zipped SQLite databases.
type SpecResponse struct {
	Response struct {
		Version                  string
		MobileAssetContentPath   string
		MobileGearAssetDataBases []struct {
			Version int
			Path    string
		}
		MobileWorldContentPaths map[string]string
		MobileGearCDN           map[string]string
	}
	ErrorCode       int
	ThrottleSeconds int
	ErrorStatus     string
	Message         string
}

// Row represents a single row from the input manifest database
type Row struct {
	ID   int
	JSON string
}

// BaseJSONData provides a type to wrap the fields contained within the JSON data of the manifest for each row.
type BaseJSONData struct {
	Hash               int64 `json:"hash"`
	*DisplayProperties `json:"displayProperties"`
}

// DisplayProperties represents almost any piece of the API that is
// intended to be displayed on the screen in some way.
type DisplayProperties struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
	HighResIcon string `json:"highResIcon"`
	HasIcon     bool   `json:"hasIcon"`
}

// ItemDefinition stores all of the fields from the DestinyInventoryItemDefinitions table
// that we are concerned with.
type ItemDefinition struct {
	BaseJSONData
	ItemType         int    `json:"itemType"`
	ItemTypeName     string `json:"itemTypeDisplayName"`
	ClassType        int    `json:"classType"`
	Equippable       bool   `json:"equippable"`
	DisplaySource    string `json:"displaySource"`
	NonTransferrable bool   `json:"nonTransferrable"`
	Inventory        struct {
		TierType       int    `json:"tierType"`
		TierTypeName   string `json:"tierTypeName"`
		MaxStackSize   int    `json:"maxStackSize"`
		BucketTypeHash int64  `json:"bucketTypeHash"`
	} `json:"inventory"`
}

// BucketDefinition defines the fields extracted from the BucketDefinition rows
type BucketDefinition struct {
	BaseJSONData
}

// ActivityModifierDefinition defines the fields extracted from the ActivityModifierDefinition rows
type ActivityModifierDefinition struct {
	BaseJSONData
}

// ActivityTypeDefinition defines the fields extracted from the ActivityTypeDefinition rows
type ActivityTypeDefinition struct {
	BaseJSONData
}

// ActivityModeDefintion defines the fields extracted from the ActivityModeDefintion rows
type ActivityModeDefintion struct {
	BaseJSONData
	ModeType    int  `json:"modeType"`
	Category    int  `json:"activityModeCategory"`
	Tier        int  `json:"tier"`
	IsAggregate bool `json:"isAggregateMode"`
	IsTeamBased bool `json:"isTeamBased"`
}

// RecordDefinition defines the fields needed from the DestinyRecordDefinition table
type RecordDefinition struct {
	BaseJSONData
	TitleInfo *TitleInfo `json:"titleInfo"`
}

// TitleInfo holds specific information about titles assigned to players
type TitleInfo struct {
	HasTitle       bool              `json:"hasTitle"`
	TitlesByGender map[string]string `json:"titlesByGender"`
}

// PlaceDefinition defines the fields extracted from the PlaceDefinition rows
type PlaceDefinition struct {
	BaseJSONData
}

// DestinationDefintion defines the fields extracted from the DestinationDefintion rows
type DestinationDefintion struct {
	BaseJSONData
	PlaceHash uint `json:"placeHash"`
}

/* Remove constraints
ALTER TABLE activities DROP CONSTRAINT destination_hash_fkey;
ALTER TABLE activities DROP CONSTRAINT place_hash_fkey;
ALTER TABLE activities DROP CONSTRAINT activity_type_hash_fkey;
ALTER TABLE activities DROP CONSTRAINT direct_activity_mode_hash_fkey;
ALTER TABLE activities DROP CONSTRAINT direct_activity_mode_type_fkey;
*/

/*
CREATE TABLE activities (
	hash bigint PRIMARY KEY,
	name text NOT NULL,
	description text NOT NULL,

	light_level integer,
	destination_hash bigint,
	place_hash bigint,
	activity_type_hash bigint,
	is_playlist boolean DEFAULT FALSE,
	is_pvp boolean DEFAULT FALSE,
	direct_activity_mode_hash bigint,
	direct_activity_mode_type integer,
	activity_mode_hashes json,
	activity_mode_types json,
	rewards json,
	modifiers json,
	challenges json,
	matchmaking json
);
*/

/* Add constraints
ALTER TABLE activities ADD CONSTRAINT destination_hash_fkey FOREIGN KEY (destination_hash) REFERENCES destinations (hash);
ALTER TABLE activities ADD CONSTRAINT place_hash_fkey FOREIGN KEY (place_hash) REFERENCES places (hash);
ALTER TABLE activities ADD CONSTRAINT activity_type_hash_fkey FOREIGN KEY (activity_type_hash) REFERENCES activity_types (hash);
ALTER TABLE activities ADD CONSTRAINT direct_activity_mode_hash_fkey FOREIGN KEY (direct_activity_mode_hash) REFERENCES activity_modes (hash)
ALTER TABLE activities ADD CONSTRAINT direct_activity_mode_type_fkey FOREIGN KEY (direct_activity_mode_type) REFERENCES activity_modes (mode_type);
*/

// ActivityDefinition defines the fields extracted from the ActivityDefinition rows
type ActivityDefinition struct {
	BaseJSONData
	LightLevel             uint   `json:"activityLightLevel"`
	DestinationHash        uint   `json:"destinationHash"`
	PlaceHash              uint   `json:"placeHash"`
	ActivityTypeHash       uint   `json:"activityTypeHash"`
	IsPlaylist             bool   `json:"isPlaylist"`
	IsPVP                  bool   `json:"isPvP"`
	DirectActivityModeHash uint   `json:"directActivityModeHash"`
	DirectActivityModeType int    `json:"directActivityModeType"`
	ActivityModeHashes     []uint `json:"activityModeHashes"`
	ActivityModeTypes      []int  `json:"activityModeTypes"`
	PGCRImage              string `json:"pgcrImage"`

	// TODO: Find the actual types for these, not just interface{}
	Rewards []*struct {
		RewardItems *struct {
			ItemHash uint `json:"itemHash"`
			Quantity uint `json:"quantity"`
		} `json:"rewardItems"`
	} `json:"rewards"`
	Modifiers []*struct {
		ActivityModifierHash uint `json:"activityModifierHash"`
	} `json:"modifiers"`
	Challenges []*struct {
		RewardSiteHash           uint `json:"rewardSiteHash"`
		InhibitRewardsUnlockHash uint `json:"inhibitRewardsUnlockHash"`
		ObjectiveHash            uint `json:"objectiveHash"`
	} `json:"challenges"`
	PlaylistItems []*struct {
		ActivityHash     uint `json:"activityHash"`
		ActivityModeHash uint `json:"activityModeHash"`
		Weight           uint `json:"weight"`
	} `json:"playlistItems"`

	Matchmaking *struct {
		IsMatchmade bool `json:"isMatchmade"`
		MinParty    int  `json:"minParty"`
		MaxParty    int  `json:"maxParty"`
		MaxPlayers  int  `json:"maxPlayers"`
		// TODO: Not sure wth this is
		RequiresGuardianOath bool `json:"requiresGuardianOath"`
	} `json:"matchmaking"`
}
