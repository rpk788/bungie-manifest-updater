package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	_ "github.com/lib/pq" // Only want to import the interface here
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/rpk788/bungie-manifest-updater/manifest"
)

var (
	// Input holds a connection to the input manifest database
	Input *InputDB
	// Output holds a connection pool to the database where the manifest data is being updated
	Output *OutputDB
)

// OutputDB represents the database that the parsed definitions will be saved
type OutputDB struct {
	Database *sql.DB
	tx       *sql.Tx
}

// InputDB represents the input database pulled down from Bungie
type InputDB struct {
	Database *sql.DB
}

// InitOutputDatabase is in charge of preparing any Statements that will be commonly used as well
// as setting up the database connection pool.
func InitOutputDatabase() error {

	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		fmt.Println("DB errror: ", err.Error())
		return err
	}

	Output = &OutputDB{
		Database: db,
	}

	return nil
}

// InitInputDatabase is in charge of preparing any Statements that will be commonly used as well
// as setting up the database connection pool.
func InitInputDatabase(dbPath string) error {

	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		fmt.Println("DB errror: ", err.Error())
		return err
	}

	Input = &InputDB{
		Database: db,
	}

	return nil
}

// GetOutputDBConnection is a helper for getting a connection to the DB based on
// environment variables or some other method.
func GetOutputDBConnection() (*OutputDB, error) {

	if Output == nil {
		fmt.Println("Initializing output db!")
		err := InitOutputDatabase()
		if err != nil {
			fmt.Println("Failed to initialize the database: ", err.Error())
			return nil, err
		}
	}

	return Output, nil
}

// GetInputDBConnection is a helper for getting a connection to the DB based on
// environment variables or some other method.
func GetInputDBConnection(path string) (*InputDB, error) {

	if Input == nil {
		fmt.Println("Initializing input db!")
		err := InitInputDatabase(path)
		if err != nil {
			fmt.Println("Failed to initialize the database: ", err.Error())
			return nil, err
		}
	} else {
		fmt.Println("Input DB already initialized")
	}

	return Input, nil
}

// ReadManifestChecksums will pull the checksums for the previously parsed
// manifests to be used for caching. If the manifest checksums match,
// there have not been any changes.
func ReadManifestChecksums() (map[string]string, error) {

	out, err := GetOutputDBConnection()
	if err != nil {
		fmt.Println("Error getting output database connection to read manifest checksums: ", err.Error())
		return nil, err
	}

	result := make(map[string]string)

	rows, err := out.Database.Query("SELECT * FROM manifest_checksums")
	if err != nil {
		fmt.Println("Error reading checksums from database: ", err.Error())
		return nil, err
	}

	for rows.Next() {
		var locale string
		var md5 string

		rows.Scan(&locale, &md5)

		result[locale] = md5
	}

	return result, nil
}

// GetItemDefinitions will pull all rows from the input database table
// with all of the item defintions.
func (in *InputDB) GetItemDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyInventoryItemDefinition")

	return rows, err
}

// GetBucketDefinitions is responsible for reading all of the bucket definitions
// out of the Destiny manifest database and returning the rows.
func (in *InputDB) GetBucketDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyInventoryBucketDefinition")

	return rows, err
}

// GetActivityModifierDefinitions is responsible for reading all of the modifiers
// out of the manifest database.
func (in *InputDB) GetActivityModifierDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyActivityModifierDefinition")

	return rows, err
}

// GetActivityTypeDefinitions is responsible for reading all of the modifiers
// out of the manifest database.
func (in *InputDB) GetActivityTypeDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyActivityTypeDefinition")

	return rows, err
}

// GetActivityModeDefinitions is responsible for reading all of the activity modes
// out of the manifest database.
func (in *InputDB) GetActivityModeDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyActivityModeDefinition")

	return rows, err
}

// GetActivityDefinitions is responsible for reading all of the activities
// out of the manifest database.
func (in *InputDB) GetActivityDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyActivityDefinition")

	return rows, err
}

// GetDestinationDefinitions is responsible for reading all of the destinations
// out of the manifest database.
func (in *InputDB) GetDestinationDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyDestinationDefinition")

	return rows, err
}

// GetPlaceDefinitions is responsible for reading all of the places
// out of the manifest database.
func (in *InputDB) GetPlaceDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyPlaceDefinition")

	return rows, err
}

// GetRecordDefinitions is responsible for reading all of the records
// out of the manifest database.
func (in *InputDB) GetRecordDefinitions() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyRecordDefinition")

	return rows, err
}

// GetGearAssetsDefinition will request all rows from the assets table and return the rows.
func (in *InputDB) GetGearAssetsDefinition() (*sql.Rows, error) {

	rows, err := in.Database.Query("SELECT * FROM DestinyGearAssetsDefinition")

	return rows, err
}

// Tables will return all of the table names from the input manifest database.
func (in *InputDB) Tables() ([]string, error) {

	rows, err := in.Database.Query("SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%'")
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	tableNames := make([]string, 0, 25)
	for rows.Next() {
		name := ""
		rows.Scan(&name)
		tableNames = append(tableNames, name)
	}

	return tableNames, nil
}

// UpdateTableSchema will alter the table specified by the name to have the new required columns needed for indexing the data.
func (in *InputDB) UpdateTableSchema(name string) error {

	_, err := in.Database.Exec("ALTER TABLE " + name + " ADD COLUMN hash INTEGER")
	if err != nil {
		return err
	}

	_, err = in.Database.Exec("ALTER TABLE " + name + " ADD COLUMN name TEXT")
	if err != nil {
		return err
	}

	return nil
}

// ExtractInterestingFields will pull the fields out of the JSON payload that can be used to quickly query entries.
func (in *InputDB) ExtractInterestingFields(tableName string) error {

	rows, err := in.Database.Query("SELECT id, json FROM " + tableName)
	if err != nil {
		return err
	}

	manifestRows := make([]manifest.Row, 0, 50)
	for rows.Next() {

		row := manifest.Row{}
		err = rows.Scan(&row.ID, &row.JSON)
		if err != nil {
			return err
		}

		manifestRows = append(manifestRows, row)
	}
	rows.Close()

	stmt, err := in.Database.Prepare("UPDATE " + tableName + " SET hash = ?, name = ? WHERE id = ?")
	if err != nil {
		return err
	}

	in.Database.Exec("BEGIN TRANSACTION")
	defer in.Database.Exec("END TRANSACTION")
	for _, r := range manifestRows {
		field := manifest.BaseJSONData{}
		err = json.Unmarshal([]byte(r.JSON), &field)
		if err != nil {
			return err
		}

		name := ""
		if field.DisplayProperties != nil {
			name = field.DisplayProperties.Name
		}
		_, err = stmt.Exec(field.Hash, name, r.ID)
		if err != nil {
			return err
		}
	}

	return nil
}

// SaveManifestChecksum is responsible for persisting the checksum for the
// specified locale to be used for caching later.
func (out *OutputDB) SaveManifestChecksum(locale, checksum string) error {

	_, err := out.Database.Exec("UPDATE manifest_checksums SET md5=$1, last_updated=NOW() WHERE locale=$2", checksum, locale)
	return err
}

// OpenTransaction begins a new transaction on the calling output database
func (out *OutputDB) OpenTransaction() error {

	var err error
	out.tx, err = out.Database.Begin()
	return err
}

// CommitTransaction commits the active transaction on the output database
func (out *OutputDB) CommitTransaction() error {

	return out.tx.Commit()
}

// DumpNewItemDefinitions will take all of the item definitions parsed out of the latest
// manifest from Bungie and write them to the output database to be used for item lookups later.
func (out *OutputDB) DumpNewItemDefinitions(locale, checksum string, definitions []*manifest.ItemDefinition) error {

	newTableTempName := fmt.Sprintf("items_%s", checksum)

	// Inside a transaction we need to Insert all item definitions into a new DB and then rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"items\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (item_hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName + " (item_hash, item_name, item_type, item_type_name, tier_type, tier_type_name, class_type, equippable, max_stack_size, display_source, non_transferrable, bucket_type_hash, icon) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	tx, err := out.Database.Begin()
	if err != nil {
		fmt.Println("Error opening transaction to output DB: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := tx.Stmt(stmt)
	for _, def := range definitions {
		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.Name), def.ItemType, strings.ToLower(def.ItemTypeName), def.Inventory.TierType, strings.ToLower(def.Inventory.TierTypeName), def.ClassType, def.Equippable, def.Inventory.MaxStackSize, def.DisplaySource, def.NonTransferrable, def.Inventory.BucketTypeHash, def.Icon)
		if err != nil {
			fmt.Println("Error inserting item definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	tx.Exec("ALTER TABLE \"items\" RENAME TO \"items_old\"")

	// Rename new temp table with permanent table name
	tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"items\"")

	// Drop old table
	tx.Exec("DROP TABLE \"items_old\"")

	// Commit or Rollback if there were errors
	err = tx.Commit()
	if err != nil {
		fmt.Println("Error commiting transaction for inserting new item definitions: ", err.Error())
		return err
	}

	return nil
}

// DumpNewBucketDefinitions will take all of the bucket definitions parsed out of the latest
// manifest from Bungie and write them to the output database to be used for bucket lookups later.
func (out *OutputDB) DumpNewBucketDefinitions(locale, checksum string, definitions []*manifest.BucketDefinition) error {

	newTableTempName := fmt.Sprintf("buckets_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"buckets\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (bucket_hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName + " (bucket_hash, name,description) VALUES($1, $2, $3)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}
	tx, err := out.Database.Begin()
	if err != nil {
		fmt.Println("Error opening transaction to output DB: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := tx.Stmt(stmt)
	for _, def := range definitions {
		if def.Name == "" {
			continue
		}

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.DisplayProperties.Name),
			def.DisplayProperties.Description)
		if err != nil {
			fmt.Println("Error inserting bucket definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	tx.Exec("ALTER TABLE \"buckets\" RENAME TO \"buckets_old\"")

	// Rename new temp table with permanent table name
	tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"buckets\"")

	// Drop old table
	tx.Exec("DROP TABLE \"buckets_old\"")

	// Commit or Rollback if there were errors
	err = tx.Commit()
	if err != nil {
		fmt.Println("Error commiting transaction for inserting new bucket definitions: ", err.Error())
		return err
	}

	return nil
}

// DumpNewActivityModifierDefinitions will take all of the bucket definitions parsed
// out of the latest manifest from Bungie and write them to the output database to be
// used for bucket lookups later.
func (out *OutputDB) DumpNewActivityModifierDefinitions(locale, checksum string, definitions []*manifest.ActivityModifierDefinition) error {

	newTableTempName := fmt.Sprintf("activity_modifiers_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then
	// rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"activity_modifiers\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName + " (hash, name, description) VALUES($1, $2, $3)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := out.tx.Stmt(stmt)
	for _, def := range definitions {
		if def.DisplayProperties.Name == "" {
			continue
		}

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.DisplayProperties.Name),
			def.DisplayProperties.Description)
		if err != nil {
			fmt.Println("Error inserting activity modifier definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	out.tx.Exec("ALTER TABLE \"activity_modifiers\" RENAME TO \"activity_modifiers_old\"")

	// Rename new temp table with permanent table name
	out.tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"activity_modifiers\"")

	// Drop old table
	out.tx.Exec("DROP TABLE \"activity_modifiers_old\"")

	return nil
}

// DumpNewActivityTypeDefinitions will take all of the activity type definitions parsed
// out of the latest manifest from Bungie and write them to the output database to be
// used for activity type lookups later.
func (out *OutputDB) DumpNewActivityTypeDefinitions(locale, checksum string, definitions []*manifest.ActivityTypeDefinition) error {

	newTableTempName := fmt.Sprintf("activity_types_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then
	// rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"activity_types\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName + " (hash, name, description) VALUES($1, $2, $3)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := out.tx.Stmt(stmt)
	for _, def := range definitions {
		if def.DisplayProperties.Name == "" {
			continue
		}

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.DisplayProperties.Name),
			def.DisplayProperties.Description)
		if err != nil {
			fmt.Println("Error inserting activity type definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	_, err = out.tx.Exec("ALTER TABLE \"activity_types\" RENAME TO \"activity_types_old\"")
	if err != nil {
		fmt.Println("Error inserting activity type definition: ", err.Error())
	}

	// Rename new temp table with permanent table name
	_, err = out.tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"activity_types\"")
	if err != nil {
		fmt.Println("Error inserting activity type definition: ", err.Error())
	}

	// Drop old table
	_, err = out.tx.Exec("DROP TABLE \"activity_types_old\"")
	if err != nil {
		fmt.Println("Error inserting activity type definition: ", err.Error())
	}

	return nil
}

// DumpNewActivityModeDefinitions will take all of the activity mode definitions parsed
// out of the latest manifest from Bungie and write them to the output database to be
// used for activity type lookups later.
func (out *OutputDB) DumpNewActivityModeDefinitions(locale, checksum string, definitions []*manifest.ActivityModeDefintion) error {

	newTableTempName := fmt.Sprintf("activity_modes_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then
	// rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"activity_modes\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName +
		" (hash, name, description, mode_type, category, tier, is_aggregate, is_team_based)" +
		" VALUES($1, $2, $3, $4, $5, $6, $7, $8)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := out.tx.Stmt(stmt)
	for _, def := range definitions {
		if def.DisplayProperties.Name == "" {
			continue
		}

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.DisplayProperties.Name),
			def.DisplayProperties.Description, def.ModeType, def.Category, def.Tier, def.IsAggregate, def.IsTeamBased)
		if err != nil {
			fmt.Println("Error inserting activity mode definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	_, err = out.tx.Exec("ALTER TABLE \"activity_modes\" RENAME TO \"activity_modes_old\"")
	if err != nil {
		fmt.Println("Error inserting activity mode definition: ", err.Error())
	}

	// Rename new temp table with permanent table name
	_, err = out.tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"activity_modes\"")
	if err != nil {
		fmt.Println("Error inserting activity mode definition: ", err.Error())
	}

	// Drop old table
	_, err = out.tx.Exec("DROP TABLE \"activity_modes_old\"")
	if err != nil {
		fmt.Println("Error inserting activity mode definition: ", err.Error())
	}

	return nil
}

// DumpNewPlaceDefinitions will take all of the place definitions parsed
// out of the latest manifest from Bungie and write them to the output database to be
// used for activity type lookups later.
func (out *OutputDB) DumpNewPlaceDefinitions(locale, checksum string, definitions []*manifest.PlaceDefinition) error {

	newTableTempName := fmt.Sprintf("places_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then
	// rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"places\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName + " (hash, name, description) VALUES($1, $2, $3)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := out.tx.Stmt(stmt)
	for _, def := range definitions {
		if strings.ToLower(def.Name) == "classified" {
			continue
		}

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.Name),
			def.DisplayProperties.Description)
		if err != nil {
			fmt.Println("Error inserting place definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	out.tx.Exec("ALTER TABLE \"places\" RENAME TO \"places_old\"")

	// Rename new temp table with permanent table name
	out.tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"places\"")

	// Drop old table
	out.tx.Exec("DROP TABLE \"places_old\"")

	return nil
}

// DumpNewDestinationDefinitions will take all of the destination definitions parsed
// out of the latest manifest from Bungie and write them to the output database to be
// used for activity type lookups later.
func (out *OutputDB) DumpNewDestinationDefinitions(locale, checksum string, definitions []*manifest.DestinationDefintion) error {

	newTableTempName := fmt.Sprintf("destinations_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then
	// rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"destinations\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName + " (hash, name, description, place_hash) VALUES($1, $2, $3, $4)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := out.tx.Stmt(stmt)
	for _, def := range definitions {
		// NOTE: Apparently this might be acceptable a lot of destinations have no name
		// if def.Name == "" {
		// 	continue
		// }

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.DisplayProperties.Name),
			def.DisplayProperties.Description, def.PlaceHash)
		if err != nil {
			fmt.Println("Error inserting destination definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	out.tx.Exec("ALTER TABLE \"destinations\" RENAME TO \"destinations_old\"")

	// Rename new temp table with permanent table name
	out.tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"destinations\"")

	// Drop old table
	out.tx.Exec("DROP TABLE \"destinations_old\"")

	return nil
}

// DumpNewRecordDefinitions will take all of the record definitions parsed
// out of the latest manifest from Bungie and write them to the output database to be
// used for activity type lookups later.
func (out *OutputDB) DumpNewRecordDefinitions(locale, checksum string, definitions []*manifest.RecordDefinition) error {

	newTableTempName := fmt.Sprintf("records_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then
	// rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"records\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName + " (hash, name, description, has_title, masculine_title, feminine_title) VALUES($1, $2, $3, $4, $5, $6)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := out.tx.Stmt(stmt)
	for _, def := range definitions {
		// NOTE: Apparently this might be acceptable a lot of destinations have no name
		// if def.Name == "" {
		// 	continue
		// }

		if def.TitleInfo == nil || def.TitleInfo.TitlesByGender == nil {
			continue
		}

		masculine, ok := def.TitleInfo.TitlesByGender["Masculine"]
		if !ok {
			masculine, ok = def.TitleInfo.TitlesByGender["Male"]
			if !ok && def.TitleInfo.HasTitle {
				log.Printf("Error Male title not found for hash=%d\n titlesByGender=%+v", def.Hash, def.TitleInfo.TitlesByGender)
			}
		}
		feminine, ok := def.TitleInfo.TitlesByGender["Feminine"]
		if !ok {
			feminine, ok = def.TitleInfo.TitlesByGender["Female"]
			if !ok && def.TitleInfo.HasTitle {
				log.Printf("Error Female title not found for hash=%d titlesByGender=%+v\n", def.Hash, def.TitleInfo.TitlesByGender)
			}
		}

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.DisplayProperties.Name),
			def.DisplayProperties.Description, def.HasIcon, masculine, feminine)
		if err != nil {
			fmt.Println("Error inserting record definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	out.tx.Exec("ALTER TABLE \"records\" RENAME TO \"records_old\"")

	// Rename new temp table with permanent table name
	out.tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"records\"")

	// Drop old table
	out.tx.Exec("DROP TABLE \"records_old\"")

	return nil
}

// DumpNewActivityDefinitions will take all of the activity definitions parsed out of the manifest
// and write them to a new table that is a flattened out version of the JSON in the sqlite database.
func (out *OutputDB) DumpNewActivityDefinitions(locale, checksum string, definitions []*manifest.ActivityDefinition) error {

	newTableTempName := fmt.Sprintf("activities_%s", checksum)

	// Inside a transaction we need to Insert all bucket definitions into a new DB and then
	// rename the old db, rename the new one, delete the old one.
	// TODO: https://dba.stackexchange.com/questions/100779/how-to-atomically-replace-table-data-in-postgresql

	// Create temp new table
	out.Database.Exec("CREATE TABLE " + newTableTempName + "(LIKE \"activities\")")
	out.Database.Exec("ALTER TABLE " + newTableTempName + " ADD PRIMARY KEY (hash)")

	stmt, err := out.Database.Prepare("INSERT INTO " + newTableTempName +
		" (hash, name, description, light_level, destination_hash, place_hash, activity_type_hash, is_playlist," +
		" is_pvp, direct_activity_mode_hash, direct_activity_mode_type, activity_mode_hashes, activity_mode_types, " +
		" rewards, modifiers, challenges, matchmaking, pgcr_image)" +
		" VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18)")
	if err != nil {
		fmt.Println("Error preparing insert statement: ", err.Error())
		return err
	}

	// Insert all rows into the temp new table
	txStmt := out.tx.Stmt(stmt)
	for _, def := range definitions {
		if def.Name == "" || strings.ToLower(def.Name) == "classified" {
			continue
		}

		activityModeHashesBytes, err := json.Marshal(def.ActivityModeHashes)
		activityModeTypeBytes, err := json.Marshal(def.ActivityModeTypes)
		rewardsBytes, err := json.Marshal(def.Rewards)
		modifiersBytes, err := json.Marshal(def.Modifiers)
		challengesBytes, err := json.Marshal(def.Challenges)
		matchmakingBytes, err := json.Marshal(def.Matchmaking)

		_, err = txStmt.Exec(def.Hash, strings.ToLower(def.Name),
			def.Description, def.LightLevel, def.DestinationHash, def.PlaceHash, def.ActivityTypeHash,
			def.IsPlaylist, def.IsPVP, def.DirectActivityModeHash, def.DirectActivityModeType,
			string(activityModeHashesBytes), string(activityModeTypeBytes), string(rewardsBytes),
			string(modifiersBytes), string(challengesBytes), string(matchmakingBytes), def.PGCRImage)

		if err != nil {
			fmt.Println("Error inserting activity definition: ", err.Error())
		}
	}

	// Rename existing table with _old suffix
	out.tx.Exec("ALTER TABLE \"activities\" RENAME TO \"activities_old\"")

	// Rename new temp table with permanent table name
	out.tx.Exec("ALTER TABLE " + newTableTempName + " RENAME TO \"activities\"")

	// Drop old table
	out.tx.Exec("DROP TABLE \"activities_old\"")

	return nil
}

// RemoveActivityConstraints will remove the constraints from the activities table while new values are removed/added so the queries succeed
func (out *OutputDB) RemoveActivityConstraints() error {

	_, err := out.tx.Exec("ALTER TABLE activities DROP CONSTRAINT destination_hash_fkey")
	if err != nil {
		return err
	}
	_, err = out.tx.Exec("ALTER TABLE activities DROP CONSTRAINT place_hash_fkey")
	if err != nil {
		return err
	}
	_, err = out.tx.Exec("ALTER TABLE activities DROP CONSTRAINT activity_type_hash_fkey")
	if err != nil {
		return err
	}

	//out.tx.Exec("ALTER TABLE activities DROP CONSTRAINT direct_activity_mode_hash_fkey")
	// out.tx.Exec("ALTER TABLE activities DROP CONSTRAINT direct_activity_mode_type_fkey")
	return nil
}

// AddActivityConstraints alters the activities table to include the proper foreign key constraints
func (out *OutputDB) AddActivityConstraints() error {

	var err error
	_, err = Output.tx.Exec("ALTER TABLE activities ADD CONSTRAINT destination_hash_fkey FOREIGN KEY (destination_hash) REFERENCES destinations (hash)")
	if err != nil {
		return err
	}
	_, err = Output.tx.Exec("ALTER TABLE activities ADD CONSTRAINT place_hash_fkey FOREIGN KEY (place_hash) REFERENCES places (hash)")
	if err != nil {
		return err
	}
	_, err = Output.tx.Exec("ALTER TABLE activities ADD CONSTRAINT activity_type_hash_fkey FOREIGN KEY (activity_type_hash) REFERENCES activity_types (hash)")
	if err != nil {
		return err
	}
	// _, err = output.tx.Exec("ALTER TABLE activities ADD CONSTRAINT direct_activity_mode_hash_fkey FOREIGN KEY (direct_activity_mode_hash) REFERENCES activity_modes (hash)")
	// if err != nil {
	// 	panic(err)
	// }
	// _, err = output.tx.Exec("ALTER TABLE activities ADD CONSTRAINT direct_activity_mode_type_fkey FOREIGN KEY (direct_activity_mode_type) REFERENCES activity_modes (mode_type)")
	// if err != nil {
	// 	panic(err)
	// }

	return nil
}

// DumpGearAssetsDefinitions will copy all asset definitions from the provided rows into the output database
func (out *OutputDB) DumpGearAssetsDefinitions(definitions []*manifest.Row) error {

	out.Database.Exec("DELETE FROM assets WHERE true")

	stmt, err := out.Database.Prepare("INSERT INTO assets (id, json) VALUES ($1, $2)")
	if err != nil {
		return err
	}

	for _, def := range definitions {
		_, err = stmt.Exec(uint32(def.ID), def.JSON)

		if err != nil {
			fmt.Printf("Failed to insert row into assets defintiions: %s\n", err.Error())
		}
	}

	return nil
}
