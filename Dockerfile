FROM golang:1.19.0-alpine

WORKDIR /go/src/gitlab.com/rpk788/bungie-manifest-updater

RUN apk add --update gcc musl-dev make

COPY . .

# No `go get` here because dependencies are/should-be vendored. Plus there is no git in this image

RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /bin/app 
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /bin/fixup cmd/fixup/main.go
