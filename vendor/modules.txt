# github.com/lib/pq v0.0.0-20170810061220-e42267488fe3
## explicit
github.com/lib/pq
github.com/lib/pq/oid
# github.com/mattn/go-sqlite3 v1.2.1-0.20170901084005-05548ff55570
## explicit
github.com/mattn/go-sqlite3
# github.com/pkg/errors v0.8.2-0.20190227000051-27936f6d90f9
## explicit
github.com/pkg/errors
# golang.org/x/net v0.0.0-20170828231752-66aacef3dd8a
## explicit
golang.org/x/net/context
# golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
## explicit
golang.org/x/sync/errgroup
